package com.company;

import java.util.Scanner;
import static com.company.JDBC.connect;
import static com.company.JDBC.eventHandler;
import static com.company.JDBC.fillBD;


public class Main
{
    public static void main(String[] args)
    {

        connect();
        System.out.println("Enter the number of rows:");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        fillBD(n);
        System.out.println("Available query: \n /price *title* \n /change_price *title* *cost* \n /filter_by_price *cost_min* *cost_max* \n /exit");
        eventHandler();

    }
}