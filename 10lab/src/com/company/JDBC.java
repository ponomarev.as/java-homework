package com.company;

import java.sql.*;
import java.util.InputMismatchException;
import java.util.Scanner;

public class JDBC
{
    private static Connection connection;
    private static Statement stmt;
    private static PreparedStatement ps;
    private static Scanner in = new Scanner (System.in);
    private static ResultSet rs;

    public static void eventHandler()
    {
        while (true)
        {
            System.out.println("Enter your query: ");
            String s = in.next();

            switch (s)
            {
                case "/price":
                    s = in.next();
                    if (isExistItem(s))
                    {
                        try
                        {
                            System.out.println(rs.getInt(2));
                        }
                        catch (SQLException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        System.out.println("goods doesn't exist");
                    }
                    break;
                case "/change_price":
                    s = in.next();
                    int newPrice = in.nextInt();
                    if (isExistItem(s))
                    {
                        try
                        {
                            stmt.executeUpdate("UPDATE 10lab SET cost = " + newPrice + " WHERE title = '" + s + "';");
                            System.out.println("Price has been changed");
                        }
                        catch (SQLException e)
                        {
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        System.out.println("goods doesn't exist");
                    }
                    break;
                case "/filter_by_price":
                    try
                    {
                        float num1 = in.nextFloat();
                        float num2 = in.nextFloat();
                        if ((num1 > 0) & (num2 > 0))
                        {
                            rs = stmt.executeQuery("SELECT title, cost FROM 10lab WHERE cost BETWEEN " + num1 + " AND " + num2);
                            if (rs.next())
                            {
                                while (rs.next())
                                {
                                    System.out.println(rs.getString(1) + ", " + rs.getInt(2));
                                }
                            }
                            else
                            {
                                System.out.println("Your query returned no results");
                            }
                        }
                        else
                        {
                            System.out.println("Enter positive numbers");
                        }
                    }
                    catch (SQLException e1)
                    {
                        e1.printStackTrace();
                    }
                    catch (InputMismatchException e2)
                    {
                        System.out.println("invalid numbers entered");

                    }
                    break;
                case "/exit":
                    System.exit(0);
                    disconnect();
            }
            in.nextLine();
        }
    }

    public static void fillBD(int n)
    {
        try
        {
            stmt = connection.createStatement();
            stmt.execute("CREATE TABLE IF NOT EXISTS mydb.10lab (id INT NOT NULL AUTO_INCREMENT, prodid INT NOT NULL, title VARCHAR(80) NOT NULL, cost DECIMAL(10,2) NOT NULL, PRIMARY KEY (id));");
            stmt.execute("DELETE FROM 10lab");
            ps = connection.prepareStatement("INSERT INTO 10lab (prodid, id, title, cost) VALUES (?, ?, ?, ?)");

            for (int i = 1; i <= n; i++)
            {
                ps.setInt(1, i * 399);
                ps.setInt(2, i);
                ps.setString(3, "goods" + i);
                ps.setInt(4, i * 10);
                ps.execute();
            }

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    private static boolean isExistItem(String nameOfItem)
    {
        try
        {
            rs = stmt.executeQuery("SELECT title, cost FROM mydb.10lab WHERE title='" + nameOfItem + "';");
            if (rs.next())
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
        return false;
    }

    public static void connect()
    {
        try
        {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/mydb?useSSL=false", "root", "ponomarev");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private static void disconnect()
    {
        try
        {
            connection.close();
        } catch (SQLException e)
        {
            e.printStackTrace();
        }
    }
}
